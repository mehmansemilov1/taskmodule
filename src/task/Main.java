package task;

public class Main {
    public static void main(String[] args) {
        try {
            InventoryManager manager = new InventoryManager();

            manager.addItem(new InventoryItem(1, "element 1", 10, 20.99));
            manager.addItem(new InventoryItem(2, "element 2", 20, 40.99));

            InventoryItem foundItem = manager.findItem(2);
            System.out.println("Tapılan Element: " + foundItem.getName());
            manager.updateItem(new InventoryItem(1, "Yenilənmiş element 1", 20, 30.99));
            manager.updateItem(new InventoryItem(2, "Yenilənmiş element 2", 25, 50.99));

            manager.deleteItem(1);
        } catch (InvalidDataException | ItemNotFoundException | FileIOException e) {
            e.printStackTrace();
        }
    }
}
