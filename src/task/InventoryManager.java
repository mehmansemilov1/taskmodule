package task;

import java.io.*;
import java.util.*;


public class InventoryManager {


    private static final String FILE_NAME = "inventory.txt";

    public List<InventoryItem> readInventoryItems() throws FileIOException {
        List<InventoryItem> items = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(FILE_NAME))) {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] parts = line.split(",");
                int itemId = Integer.parseInt(parts[0]);
                String name = parts[1];
                int quantity = Integer.parseInt(parts[2]);
                double price = Double.parseDouble(parts[3]);
                items.add(new InventoryItem(itemId, name, quantity, price));
            }
        } catch (IOException e) {
            throw new FileIOException("Error inventory(writing) file:  " + e.getMessage());
        }
        return items;
    }

    public void writeInventoryItems(List<InventoryItem> items) throws FileIOException {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(FILE_NAME))) {
            for (InventoryItem item : items) {
                writer.write(String.format("%d,%s,%d,%.2f%n", item.getItemId(), item.getName(), item.getQuantity(), item.getPrice()));
            }
        } catch (IOException e) {
            throw new FileIOException("Error inventory(writing) file: " + e.getMessage());
        }
    }

    public void addItem(InventoryItem item) throws InvalidDataException, FileIOException {
        List<InventoryItem> items = readInventoryItems();
        items.add(item);
        writeInventoryItems(items);
    }

    public InventoryItem findItem(int itemId) throws ItemNotFoundException, FileIOException {
        List<InventoryItem> items = readInventoryItems();
        for (InventoryItem item : items) {
            if (item.getItemId() == itemId) {
                return item;
            }
        }
        throw new ItemNotFoundException("element ID " + itemId + " tapilmadi.");
    }

    public void updateItem(InventoryItem newItem) throws InvalidDataException, ItemNotFoundException, FileIOException {
        List<InventoryItem> items = readInventoryItems();
        boolean found = false;
        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).getItemId() == newItem.getItemId()) {
                items.set(i, newItem);
                found = true;
                break;
            }
        }
        if (!found) {
            throw new ItemNotFoundException("element  ID " + newItem.getItemId() + " tapilmadi");
        }
        writeInventoryItems(items);
    }

    public void deleteItem(int itemId) throws ItemNotFoundException, FileIOException {
        List<InventoryItem> items = readInventoryItems();
        boolean found = false;
        for (Iterator<InventoryItem> iterator = items.iterator(); iterator.hasNext(); ) {
            InventoryItem item = iterator.next();
            if (item.getItemId() == itemId) {
                iterator.remove();
                found = true;
                break;
            }
        }
        if (!found) {
            throw new ItemNotFoundException("element ID " + itemId + "tapilmadi.");
        }
        writeInventoryItems(items);
    }
}
